import json
import markovify
import boto3

s3 = boto3.client('s3')

def lambda_handler(event, context):

    bucket = event['bucket']
    key = event['key']

    obj = s3.get_object(Bucket=bucket, Key=key)

    raw_text = obj['Body'].read().decode('utf-8')

    model_text = markovify.Text(raw_text, state_size=1)

    model_json = model_text.to_json()

    # Put the new model file in the models sub folder of the s3 bucket
    model_key = 'models/' + key + '_MODEL'

    resp = s3.put_object(Body=bytes(model_json, 'utf-8'), Bucket=bucket, Key=model_key)

    print(resp)
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "Success"
        }),
    }
